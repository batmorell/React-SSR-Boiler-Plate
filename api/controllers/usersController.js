const Users = require('../models/User');

module.exports = UsersRoutes = {
  createNewUser(req, res) {
    const { username, password, role } = req.body;
    var user = new Users({
      username,
      password,
      role
    });
    user.save(function (err, user) {
      if (err) return console.error(err);
      res.status(200).send('user created');
    });
  },
  getAllUsers(req, res) {
    Users.find({ role: 'user'}, (err, result) => {
      if (err) return console.log(err)
      res.status(200).send(result);
    })
  }
}