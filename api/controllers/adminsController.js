const Users = require('../models/User');

module.exports = UsersRoutes = {
  getAllAdmins(req, res) {
    Users.find({ role: 'admin'}, (err, result) => {
      if (err) return console.log(err)
      res.status(200).send(result);
    })
  }
}