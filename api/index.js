const express = require('express');
const mongoose = require('mongoose');
const session = require("express-session")
const passport = require('passport');
const bodyParser = require('body-parser');
const cors = require('cors');
const keys = require('./config/keys');
require('./services/passport');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/db');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({ secret: "HelloAuth" }));
app.use(passport.initialize());
app.use(passport.session());

require('./routes/authRoutes')(app);
require('./routes/userRoutes')(app);

const PORT = process.env.PORT || 5000;
app.listen(PORT);
