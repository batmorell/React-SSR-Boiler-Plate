const passport = require('passport');

module.exports = app => {
  app.post('/login',
    passport.authenticate('local', {
      failWithError: true
    }), (req, res) => {
      return res.status(200).send('ok');
    },  (err, req, res, next) => {
      return res.status(403).send('failed');
    }
  );

  app.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
  });

  app.get('/current_user', (req, res) => {
    res.send(req.user);
  });
};
