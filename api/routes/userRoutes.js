const mongoose = require('mongoose');
const requireLogin = require('../middlewares/requireLogin');
const requireAdmin = require('../middlewares/requireAdmin');
const userController = require('../controllers/usersController');
const adminController = require('../controllers/adminsController');

module.exports = app => {
  app.get('/users', requireLogin, userController.getAllUsers);

  app.post('/users', userController.createNewUser);

  app.get('/admins', requireAdmin, adminController.getAllAdmins);
};
