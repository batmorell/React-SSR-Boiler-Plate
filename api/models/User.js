const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const keys = require('../config/keys');
const { Schema } = mongoose;

const userSchema = new Schema({
  username: String,
  password: String,
  role: String,
});

userSchema.pre('save', function preSave(next) {
  const user = this;
  bcrypt.genSalt(keys.bcrypt.salt, (err, salt) => {
    if (err) return next(err);
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.validPassword = function( pwd ) {
  return bcrypt.compare(pwd, this.password)
};

module.exports = mongoose.model('users', userSchema);
