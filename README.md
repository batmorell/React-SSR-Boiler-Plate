# React-SSR Dashboard Boiler Plate with antd

You can find in the following repository a simple boiler plate for React project, with:

- SSR React NodeJS server
- NodeJS api server

## Setup project

### Setup API Server


#### Move to API directory
```
cd api
```

#### Install dependencies
```
npm i
```

#### Launch Mongo daemon and import database
```
mongod --dbpath=./db
mongoimport --db db --collection users --file users.json
```

#### Launch Node server
```
npm run dev
```

### Setup render server


#### Move to API directory
```
cd server
```

#### Install dependencies
```
npm i
```

#### Launch Node server
```
npm run dev
```
