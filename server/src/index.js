import 'babel-polyfill';
import express from 'express';
import { matchRoutes } from 'react-router-config';
import proxy from 'express-http-proxy';
import Routes from './client/Routes';
import renderer from './helpers/renderer';
import createStore from './helpers/createStore';

const app = express();

// Define here a proxy to redirect api call 
app.use(
  '/api',
  proxy('http://localhost:5000')
);

app.use(express.static('public'));

// All others routes are redirected to react router
app.get('*', (req, res) => {
  // Server side redux store
  const store = createStore(req);

  // We wait here the data loading
  // Map on route, load data and return a promise
  // matchRoutes is return by react-router-config and return an array of matching route and components
  // promises will contain a list of promises from the store, they will be resolve when data is loaded
  const promises = matchRoutes(Routes, req.path)
    .map(({ route }) => {
      // If loadData exist we call it
      // The loadData function has to be define into the component
      return route.loadData ? route.loadData(store) : null;
    })
    .map(promise => {
      if (promise) {
        return new Promise((resolve, reject) => {
          promise.then(resolve).catch(resolve);
        });
      }
    });

  // Wait for all promises comming from previous function
  Promise.all(promises).then(() => {
    const context = {};
    const content = renderer(req, store, context);

    if (context.url) {
      return res.redirect(301, context.url);
    }
    if (context.notFound) {
      res.status(404);
    }

    res.send(content);
  });
});

app.listen(3000, () => {
  console.log('Listening on port 3000');
});
