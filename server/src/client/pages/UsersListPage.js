import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import { Button } from 'antd';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { fetchUsers } from '../actions';


class UsersList extends Component {
  componentDidMount() {
    this.props.fetchUsers();
  }

  renderUsers() {
    return this.props.users.map(user => <li key={user.id}>{user.name}</li>);
  }

  head() {
    return (
      <Helmet>
        <title>{`${this.props.users.length} Users Loaded`}</title>
        <meta property="og:title" content="Users App" />
      </Helmet>
    );
  }

  render() {
    return (
      <div>
        {this.head()}
        Here's a big list of users:
        <ul>{this.renderUsers()}</ul>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { users: state.users };
}

// Put here data that need to be load before render the component
function loadData(store) {
  return store.dispatch(fetchUsers());
}

// export loadData and component for Routes functions
export default {
  loadData,
  component: connect(mapStateToProps, { fetchUsers })(UsersList)
};
