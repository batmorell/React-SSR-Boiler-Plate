import React, { Component } from 'react';
import { Redirect } from 'react-router'
import axios from 'axios';

class Home extends Component {

  render() {
    return (
      <div className="center-align">
        <h3>Welcome to WAS Station</h3>
      </div>
    );
  }
};

export default {
  component: Home
};
