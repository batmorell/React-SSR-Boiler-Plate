import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import axios from 'axios';
import { fetchCurrentUser } from '../actions';
import { Form, Icon, Input, Button, Layout, Row, Col } from 'antd';

const { Header, Content, Footer } = Layout;
const FormItem = Form.Item;

class Login extends Component {
  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        axios.post('api/login', values)
          .then(() => {
            this.props.fetchCurrentUser();
            this.props.history.push('/');
          })
          .catch(() => this.props.history.push('/login'));
      }
    });
  }

  hasErrors = (fieldsError) => {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
  }

  head() {
    return (
      <Helmet>
        <title>{`WAS - Login`}</title>
        <meta property="og:title" content="WAS - Login" />
      </Helmet>
    );
  }

  render() {
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
    
    const userNameError = isFieldTouched('username') && getFieldError('username');
    const passwordError = isFieldTouched('password') && getFieldError('password');
    return (
      <Layout>
        {this.head()}
        <Content>
          <Row type="flex" justify="center" align="middle" style={{ minHeight: '100vh' }}>
            <Col span={8}>
              <img src="img/logo-small.png" style={{ width: '100%' }}/>
              <Form layout="horizontal" onSubmit={this.handleSubmit}>
                <FormItem
                  validateStatus={userNameError ? 'error' : ''}
                  help={userNameError || ''}
                >
                  {getFieldDecorator('username', {
                    rules: [{ required: true, message: 'Vous devez entrer votre username!' }],
                  })(
                    <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Username" />
                  )}
                </FormItem>
                <FormItem
                  validateStatus={passwordError ? 'error' : ''}
                  help={passwordError || ''}
                >
                  {getFieldDecorator('password', {
                    rules: [{ required: true, message: 'Il manque votre password :(' }],
                  })(
                    <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Password" />
                  )}
                </FormItem>
                <FormItem>
                  <Button
                    type="primary"
                    htmlType="submit"
                    disabled={this.hasErrors(getFieldsError())}
                  >
                    Connexion
                  </Button>
                </FormItem>
              </Form>
            </Col>
          </Row>
        </Content>
      </Layout>
    );
  }
}

const WrappedLogin = Form.create()(Login);

function mapStateToProps(state) {
  return { auth: state.auth };
}

export default {
  component: connect(mapStateToProps, { fetchCurrentUser })(WrappedLogin)
};
