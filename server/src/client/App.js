import React from 'react';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { Link } from 'react-router-dom';
import { Layout, Menu, Breadcrumb, Icon } from 'antd';
import requireAuth from './components/hocs/requireAuth';
import { fetchCurrentUser } from './actions';
import Sider from './components/Sider';

const { Header, Content, Footer } = Layout;
const SubMenu = Menu.SubMenu;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
    };
  }

  componentWillMount() {
    this.props.fetchCurrentUser()
  }

  onCollapse(collapsed) {
    this.setState({ collapsed });
  }

  render() {
    return(
      <Layout style={{ minHeight: '100vh' }}>
        <Sider />
        <Layout>
          <Header />
          <Content>
            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
              {renderRoutes(this.props.route.routes)}
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            WAS Station ©2017 Created by WAS
          </Footer>
        </Layout>
      </Layout>
    );
  }
};

function mapStateToProps({ auth }) {
  return { auth };
}

export default {
  component: connect(mapStateToProps, { fetchCurrentUser })(requireAuth(App)),
  loadData: ({ dispatch }) => dispatch(fetchCurrentUser())
};
