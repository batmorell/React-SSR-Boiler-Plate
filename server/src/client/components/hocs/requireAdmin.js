import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

export default ChildComponent => {
  class RequireAuth extends Component {
    render() {
      console.log(this.props.auth);
      switch (this.props.auth.role) {
        case 'admin':
          return <ChildComponent {...this.props} />;
        default:
          return <Redirect to="/notfound" />;
      }
    }
  }

  function mapStateToProps({ auth }) {
    return { auth };
  }

  return connect(mapStateToProps)(RequireAuth);
};
