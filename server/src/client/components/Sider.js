import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu, Icon } from 'antd';

const { Sider } = Layout;
const SubMenu = Menu.SubMenu;

const SiderComp = () => {
  return (
    <Sider
      collapsible
      collapsed={false}
    >
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
        <Menu.Item key="1">
          <Link to="/"><Icon type="home" />Home</Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/users"><Icon type="user" />Users</Link>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/admins"><Icon type="user" />Admins</Link>
        </Menu.Item>
        <Menu.Item key="10">
          <a href="api/logout"><Icon type="logout" />Logout</a>
        </Menu.Item>
      </Menu>
    </Sider>
  );
};

export default SiderComp;