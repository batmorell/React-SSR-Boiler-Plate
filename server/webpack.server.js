const path = require('path');
const webpackNodeExternals = require('webpack-node-externals');

// Here is the webpack config for the server
// With as input: src/index.js
// And as output build/bundle.js
const config = {
  // Inform webpack that we're building a bundle
  // for nodeJS, rather than for the browser
  target: 'node',

  // Tell webpack the root file of our
  // server application
  entry: './src/index.js',

  // Tell webpack where to put the output file
  // that is generated
  output: {
    filename: 'bundle.js',
    // The resolve path with __dirname will use the current directory
    path: path.resolve(__dirname, 'build')
  },

  externals: [webpackNodeExternals()],
  module: {
    rules: [
      {
        // Add .jsx here to use jsx files
        test: /\.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          // preset react turn jsx into js
          // stage-0 is useful for async/await
          presets: [
            'react',
            'stage-0',
            ['env', { targets: { browsers: ['last 2 versions'] } }]
          ],
          plugins: [
            ["transform-class-properties", { "spec": true }]
          ]
        }
      },
      {
        test: /\.css$/,
        include: [/node_modules\/.*antd/],
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
          },
        ],
      },
    ]
  }
};

module.exports = config;
